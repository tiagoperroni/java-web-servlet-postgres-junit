package dao;

import db.DbConfig;
import service.UsuarioServiceImpl;

public class DAOFactory {
	
	public static UsuarioDAO createUsuarioDao() {
		return new UsuarioServiceImpl(DbConfig.conectar());
	}

}
