package dao;

import java.util.List;

import model.Usuario;

public interface UsuarioDAO {

	List<Usuario> findAll();

	Usuario findyById(Integer id);

	Integer saveUser(Usuario usuario);

	Integer updateUser(Usuario usuario);

	Integer deleteUsuario(Integer id);

}
