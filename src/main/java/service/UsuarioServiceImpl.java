package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dao.UsuarioDAO;
import db.DbConfig;
import model.Usuario;

public class UsuarioServiceImpl implements UsuarioDAO {

	private Connection conn;

	public UsuarioServiceImpl() {
		super();
	}

	public UsuarioServiceImpl(Connection conn) {
		this.conn = conn;
	}

	@Override
	public List<Usuario> findAll() {
		PreparedStatement st = null;
		ResultSet rs = null;
		List<Usuario> usuarios = new ArrayList<Usuario>();
		String getAll = "select * from usuario_db order by id";
		try {
			conn = DbConfig.conectar();
			st = conn.prepareStatement(getAll);
			rs = st.executeQuery();
			while (rs.next()) {
				int id = rs.getInt(1);
				String nome = rs.getString(2);
				String email = rs.getString(3);
				Date dataCadastro = rs.getTimestamp(4);		
				usuarios.add(new Usuario(id, nome, email, dataCadastro));
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			DbConfig.closeResultSet(rs);
			DbConfig.closeStatement(st);
		}
		return usuarios;
	}

	@Override
	public Usuario findyById(Integer id) {
		System.out.println(id);
		PreparedStatement st = null;
		ResultSet rs = null;
		String obterPorId = "select * from usuario_db where id = ?";
		try {
			conn = DbConfig.conectar();
			st = conn.prepareStatement(obterPorId);
			st.setInt(1, id);
			rs = st.executeQuery();
			if (rs.next()) {
				int idUser = rs.getInt(1);
				String nome = rs.getString(2);
				String email = rs.getString(3);
				Date dataCadastro = rs.getDate(4);
				Usuario usuario = new Usuario(idUser, nome, email, dataCadastro);
				return usuario;
			}
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			DbConfig.closeResultSet(rs);
			DbConfig.closeStatement(st);
		}
		return null;
	}

	@Override
	public Integer saveUser(Usuario usuario) {
		
		if (usuario.getNome() == null) {
			return 3;
		}
		
		else if (usuario.getEmail() == null) {
			return 4;
		}		
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		String saveUser = "insert into usuario_db (nome,email,datacadastro) values (?,?,?)";
		try {
			Connection conn = DbConfig.conectar();
			ps = conn.prepareStatement(saveUser, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, usuario.getNome());
			ps.setString(2, usuario.getEmail());
			ps.setTimestamp(3, Timestamp.valueOf("2021-03-05 13:58:22"));
			int rowsAffected = ps.executeUpdate();

			return rowsAffected;

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			DbConfig.closeResultSet(rs);
			DbConfig.closeStatement(ps);
		}
		
		return null;
	}

	@Override
	public Integer updateUser(Usuario usuario) {
		PreparedStatement ps = null;
		String update = "update usuario_db set nome = ?, email = ? where id = ?";
		try {
			conn = DbConfig.conectar();
			ps = conn.prepareStatement(update);
			ps.setString(1, usuario.getNome());
			ps.setString(2, usuario.getEmail());
			ps.setInt(3, usuario.getId());

			int response = ps.executeUpdate();
			return response;

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			DbConfig.closeStatement(ps);
		}
		return null;
	}

	@Override
	public Integer deleteUsuario(Integer id) {
		PreparedStatement ps = null;
		String delete = "delete from usuario_db where id = ?";
		try {
			Connection conn = DbConfig.conectar();
			ps = conn.prepareStatement(delete);
			ps.setInt(1, id);
			int isDeleted = ps.executeUpdate();

			return isDeleted;

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			DbConfig.closeStatement(ps);
		}
		return null;
	}
}
