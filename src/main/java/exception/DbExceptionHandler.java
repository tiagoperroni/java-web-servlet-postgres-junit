package exception;

public class DbExceptionHandler extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DbExceptionHandler(String message) {
		super(message);
	}

	public DbExceptionHandler(String message, Throwable cause) {
		super(message, cause);
	}

}
