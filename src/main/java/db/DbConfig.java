package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import exception.DbExceptionHandler;

public class DbConfig {	

	private static String driver = "org.postgresql.Driver";
	private static String url = "jdbc:postgresql://localhost:5432/postgres";
	private static String user = "postgres";
	private static String password = "1234567";

	public static Connection conectar() {
		Connection conn = null;
		if (conn == null) {
			try {
				Class.forName(driver);
				conn = DriverManager.getConnection(url, user, password);
			} catch (Exception e) {
				System.out.println(e);
				return null;
			}
		}

		return conn;
	}

	public static String testeConexao() {
		try {
			Connection conn = conectar();
			conn.close();
			return "DB CONECTADO!";
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public static void closeConection(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
				System.out.println("Conexão com DB encerrada!");
			} catch (Exception e) {
				throw new DbExceptionHandler(e.getMessage());
			}
		}
	}
	
	public static void closeStatement(Statement st) {
		if (st != null) {
			try {
				st.close();
				System.out.println("Statemente foi fechado.");
			} catch (SQLException e) {
				throw new DbExceptionHandler(e.getMessage());
			}
		}
	}
	
	public static void closeResultSet(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
				System.out.println("ResultSet foi fechado.");
			} catch (SQLException e) {
				throw new DbExceptionHandler(e.getMessage());
			}
		}
	}

}
