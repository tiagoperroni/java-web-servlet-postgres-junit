package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.DAOFactory;
import dao.UsuarioDAO;
import model.Usuario;

@WebServlet(name = "UserController", urlPatterns = { "/user", "/userid" })
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Gson gson = new Gson();

	UsuarioDAO userDao = DAOFactory.createUsuarioDao();

	@Override
	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setHeader("Access-Control-Allow-Origin", "*");
		resp.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
		resp.setHeader("Access-Control-Allow-Headers", "*");
		resp.setHeader("Access-Control-Max-Age", "86400");
		// Tell the browser what requests we allow.
		resp.setHeader("Allow", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS");
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.addHeader("Access-Control-Allow-Origin", "http://localhost:4200");
		response.addHeader("Access-Control-Allow-Methods", "GET");

		String path = request.getServletPath();
		System.out.println(path);

		if (path.equals("/userid")) {
			findById(request, response);
		} else {

			PrintWriter out = response.getWriter();

			response.setContentType("application/json");

			response.setCharacterEncoding("UTF-8");
			response.setStatus(200);
			out.print(this.gson.toJson(userDao.findAll()));
			out.flush();
		}
	}

	public void findById(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {		

		Usuario findUser = userDao.findyById(request.getIntHeader("id"));

		PrintWriter out = response.getWriter();

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		if (findUser != null) {
			response.setStatus(200);
			out.print(this.gson.toJson(findUser));
		} else {
			response.setStatus(404);
			out.print("Não foi encontrado usuario com id: " + request.getIntHeader("id"));
		}
		out.flush();

	}

	/*
	 * salva um novo usuario no banco de dados ele verifica se o campo nome ou email
	 * é null e se for ele retorna mensagem solitando o ajuste
	 */

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer sb = new StringBuffer();
		String atributo = null;

		resp.addHeader("Access-Control-Allow-Origin", "http://localhost:4200");
		resp.addHeader("Access-Control-Allow-Methods", "POST");

		BufferedReader leitor = req.getReader();
		while ((atributo = leitor.readLine()) != null) {
			sb.append(atributo);
		}

		Usuario user = gson.fromJson(sb.toString(), Usuario.class);
		PrintWriter out = resp.getWriter();
		int resposta = userDao.saveUser(user);
		resp.setContentType("application/json");

		resp.setCharacterEncoding("UTF-8");

		if (resposta == 3) {
			resp.setStatus(400);
			out.print("O campo nome não pode ser nulo.");
		}

		else if (resposta == 4) {
			resp.setStatus(400);
			out.print("O campo e-mail não pode ser nulo.");
		}

		else if (resposta > 0) {
			resp.setStatus(201);
			out.flush();
		} else {
			resp.setStatus(400);
			out.print("Não foi possível salvar o usuário.");
		}

	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.addHeader("Access-Control-Allow-Origin", "http://localhost:4200");
		response.addHeader("Access-Control-Allow-Methods", "PUT");

		StringBuffer sb = new StringBuffer();
		String atributo = null;

		BufferedReader leitor = request.getReader();
		while ((atributo = leitor.readLine()) != null) {
			sb.append(atributo);
			System.out.println(atributo);
		}

		Usuario user = gson.fromJson(sb.toString(), Usuario.class);
		user.setDataCadastro(null);
		int resposta = userDao.updateUser(user);

		PrintWriter out = response.getWriter();

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		if (resposta > 0) {
			response.setStatus(200);
		} else {
			response.setStatus(404);
			out.print("Nao foi encontrado usuário com id: " + user.getId());
		}

		out.flush();
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.addHeader("Access-Control-Allow-Origin", "http://localhost:4200");
		response.addHeader("Access-Control-Allow-Methods", "DELETE");

		int id = request.getIntHeader("id");

		int resposta = userDao.deleteUsuario(id);

		PrintWriter out = response.getWriter();

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		if (resposta > 0) {
			response.setStatus(204);
		} else {
			response.setStatus(404);
			out.print("Não foi encontrado usuário com id: " + request.getIntHeader("id"));
		}

		out.flush();
	}

}
