package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import com.google.gson.Gson;

import db.DbConfig;
import model.Usuario;

public class Test {

	public static void main(String[] args) {

	}

	Gson gson = new Gson();

	@org.junit.Test
	// @Ignore
	public void testDBConnection() {
		System.out.println();
		System.out.println("=== Teste Conexão Com Db ===");

		try {
			String response = DbConfig.testeConexao();
			assertEquals("DB CONECTADO!", response);
			System.out.println("Feito o teste de conexão");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@org.junit.Test
	// @Ignore
	public void testInstanciarUsuario() {
		System.out.println();
		System.out.println("=== TESTE INSTÂNCIA USUÁRIO ===");
		Usuario usuario = new Usuario(18, "Lucas Silva", "lucas@gmail.com", new Date());
		Usuario usuario2 = new Usuario(19, "Lucas Silva", "lucas@gmail.com", new Date());

		assertEquals("Lucas Silva", usuario.getNome());
		assertTrue(usuario.getEmail().equals("lucas@gmail.com"));
		assertNotEquals(usuario, usuario2);
		System.out.println(usuario);
	}

	@org.junit.Test
	// @Ignore
	public void testGetAll() {
		System.out.println();
		System.out.println("=== TESTE GET ALL ===");
		try {
			URL url = new URL("http://localhost:8080/treino-servlet/user");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}

			assertEquals(200, conn.getResponseCode());

			conn.disconnect();

		} catch (Exception e) {
			System.out.println("Exception in NetClientGet:- " + e);
		}
	}

	@org.junit.Test
	// @Ignore
	public void testPost() {

		System.out.println();
		Usuario usuario = new Usuario(null, "Teste Salvar", "testesalvar@gmail.com", null);
		System.out.println("=== TESTE SALVAR USUARIO ===");

		try {
			URL url = new URL("http://localhost:8080/treino-servlet/user");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection(); // abre conexao

			connection.setRequestMethod("POST"); // fala que quer um post

			connection.setRequestProperty("Content-type", "application/json"); // fala o que vai mandar

			connection.setDoOutput(true); // fala que voce vai enviar algo

			PrintStream printStream = new PrintStream(connection.getOutputStream());
			printStream.println(gson.toJson(usuario)); // seta o que voce vai enviar

			connection.connect(); // envia para o servidor

			InputStreamReader in = new InputStreamReader(connection.getInputStream()); // pega resposta
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				assertEquals("Usuario salvo com sucesso!", output);
			}

			assertEquals(201, connection.getResponseCode());

			connection.disconnect();

		} catch (Exception e) {
			System.out.println("Exception in NetClientGet:- " + e);
		}
	}

	@org.junit.Test
	//@Ignore
	public void testeAtualizar() {
		System.out.println();
		System.out.println("=== TESTE ATUALIZAR USUARIO POR ID ===");
		try {
			int idAtualizar = 31;
			Usuario usuario = new Usuario(idAtualizar, "Teste Update", "teste@gmail.com", null);

			URL url = new URL("http://localhost:8080/treino-servlet/user");
			
			HttpURLConnection connection = (HttpURLConnection) url.openConnection(); // abre conexao

			connection.setRequestMethod("PUT"); // fala que quer um post

			connection.setRequestProperty("Accept", "application/json"); // fala o que vai mandar

			connection.setDoOutput(true); // fala que voce vai enviar algo

			PrintStream printStream = new PrintStream(connection.getOutputStream());
			printStream.println(gson.toJson(usuario)); // seta o que voce vai enviar

			connection.connect(); // envia para o servidor

			InputStreamReader in = new InputStreamReader(connection.getInputStream()); // pega resposta
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				assertEquals("Usuario atualizado com sucesso.", output);
			}
			
			assertEquals(200, connection.getResponseCode());

			connection.disconnect();

		} catch (Exception e) {
			System.out.println("Exception in NetClientGet:- " + e);
		}
	}

	@org.junit.Test
	//@Ignore
	public void testDeletar() {
		System.out.println();
		System.out.println("=== TESTE DELETAR USUARIO POR ID ===");

		int idDeletar = 32;
		Usuario usuario = new Usuario(idDeletar, null, null, null);
		try {

			URL url = new URL("http://localhost:8080/treino-servlet/user");
			System.out.println(url);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection(); // abre conexao

			connection.setRequestMethod("DELETE"); // fala que quer um post

			connection.setRequestProperty("Accept", "application/json"); // fala o que vai mandar

			connection.setDoOutput(true); // fala que voce vai enviar algo

			PrintStream printStream = new PrintStream(connection.getOutputStream());
			printStream.println(gson.toJson(usuario)); // seta o que voce vai enviar

			connection.connect(); // envia para o servidor

			InputStreamReader in = new InputStreamReader(connection.getInputStream()); // pega resposta
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				assertEquals("Usuario deletado com sucesso.", output);
			}
			
			assertEquals(202, connection.getResponseCode());
			connection.disconnect();

		} catch (Exception e) {
			System.out.println("Exception in NetClientGet:- " + e);
		}
	}
}
